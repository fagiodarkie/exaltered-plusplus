# Exaltered++

![buddyworks badge](https://app.buddy.works/fagiodarkie/exaltered-plusplus/pipelines/pipeline/160575/badge.svg?token=4cabeb6b5da66f96de7538cad958cb32020e5b3507acbc846e40a69ad62af91c)
[![BCH compliance](https://bettercodehub.com/edge/badge/fagiodarkie/exaltered-plusplus?branch=master)](https://bettercodehub.com/)

C++ version of Exaltered, an interactive Character sheet for a heavily altered version of the White Wolf Publishing RPG Exalted (version 2.0).

## Project Monitoring Services

### Continuous Integration Service
Exaltered++ is developed under Continuous Integration provided by buddy.works.

### Clean Code Monitoring
Exaltered++ is monitored for Clean Code practices by Better Code Hub.

## Intellectual property and rights management
Please note that the distributed software is licensed with GNU General Public License 3.0.
The intellectual property of the software and all its parts belongs to the contributors.
The source code is published for showcasing and collaboration purposes and is not intended to be sold, licensed or otherwise exploited in commercial activities.

© 2017 White Wolf Entertainment AB.
All rights reserved. Exalted® and Storytelling System™ are trademarks and/or registered trademarks of White Wolf Entertainment AB.
All rights reserved. www.white-wolf.com
